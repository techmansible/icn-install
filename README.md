icn_install:

	1.Variables that were placed previously in common role has been moved into defaults of icn_install role
	2.Variables that are newly added in defaults:
		icn_repo_type
		icn_Was_Federated_BaseEntry_DNRepository
		cm_svr_hostkey
		cm_svr_hostname
		icn_JDBC_DataSource_Name
		icn_filenetP8_Object_Store
		icn_connection_Community_URL
		icn_connections_Community_HTTPS_URL
		icn_configmgr_generateconfig_cmd_cm_loop
		icn_xml_structure_modifier_cm_loop
		icn_applicationserver_config_contents_cm_loop
		icn_configureldap_config_contents_cm_loop
		icn_deployapplication_config_contents_cm_loop
		icn_importltpakey_config_contents_cm_loop
		icn_downloadcejarstask_config_contents_cm_loop
		icn_configurefncstask_config_contents_cm_loop
	3.Version of ICN installer has been updated from 3.0.4 to 3.0.5 in the variables icn_installation_commands & icn_artifact_list

